package main
import (
    "encoding/json"
    "log"
    "net/http"
	"io/ioutil"
    "github.com/aymerick/raymond"
    "html/template"
    "path/filepath"
)

func main() {
    http.HandleFunc("/render", renderTemplate)
	http.HandleFunc("/template/", showTemplate)
	http.HandleFunc("/templates", listTemplates)
    log.Fatal(http.ListenAndServe(":8080", nil))
}

func listTemplates(w http.ResponseWriter, r *http.Request) {
    if r.Method == http.MethodGet {
        // List all .hbs files in the templates directory
        files, err := ioutil.ReadDir("templates")
        if err != nil {
            http.Error(w, err.Error(), http.StatusInternalServerError)
            return
        }

        var templates []string
        for _, file := range files {
            if filepath.Ext(file.Name()) == ".hbs" {
                templates = append(templates, file.Name())
            }
        }

        // Render the list of templates
        w.Header().Set("Content-Type", "text/html")
        w.Write([]byte("<h1>Templates</h1>"))
        for _, template := range templates {
            w.Write([]byte("<a href=\"/templates?file=" + template + "\">" + template + "</a><br>"))
        }
    } else if r.Method == http.MethodPost {
        // Render the selected template with user-provided JSON data
        file := r.FormValue("file")
        jsonData := r.FormValue("json")

        var data map[string]interface{}
        err := json.Unmarshal([]byte(jsonData), &data)
        if err != nil {
            http.Error(w, err.Error(), http.StatusBadRequest)
            return
        }

        tmpl, err := template.ParseFiles("templates/" + file)
        if err != nil {
            http.Error(w, err.Error(), http.StatusInternalServerError)
            return
        }

        err = tmpl.Execute(w, data)
        if err != nil {
            http.Error(w, err.Error(), http.StatusInternalServerError)
            return
        }
    }
}

func renderTemplate(w http.ResponseWriter, r *http.Request) {
    // Parse the JSON data from the request body
    var data map[string]interface{}
    err := json.NewDecoder(r.Body).Decode(&data)
    if err != nil {
        http.Error(w, err.Error(), http.StatusBadRequest)
        return
    }

    // Get the template file name from the request query parameter
    templateFile := r.URL.Query().Get("template")
    if templateFile == "" {
        http.Error(w, "Missing template parameter", http.StatusBadRequest)
        return
    }

    // Read the template file
    templateContent, err := ioutil.ReadFile("templates/" + templateFile)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }
    // Render the template with the provided data
    result, err := raymond.Render(string(templateContent), data)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }

    // Set the response content type and write the rendered template
    w.Header().Set("Content-Type", "text/html")
    w.Write([]byte(result))
}


func showTemplate(w http.ResponseWriter, r *http.Request) {
	// Extract the template file name from the URL path
	file := filepath.Base(r.URL.Path)

	// Read the contents of the .hbs file
	content, err := ioutil.ReadFile("templates/" + file)
	if err != nil {
		// Set the response content type to plain text
		w.Header().Set("Content-Type", "text/plain")
		// Use http.StatusText to set the error message
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	// Set the response content type to plain text
	w.Header().Set("Content-Type", "text/plain")

	// Write the file contents as the response
	w.Write(content)
}