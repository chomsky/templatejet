# Use the official Golang image as a parent image for the build stage
FROM golang:1.21-alpine AS builder

# Set the working directory inside the container
WORKDIR /app

# Copy the local package files to the container's workspace
COPY go.mod ./
COPY go.sum ./
COPY main.go ./
COPY templates ./templates/

# Download and install the dependencies
RUN go mod download

# Build the Go executable
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

# Use a minimal base image for the final stage
FROM alpine:latest

# Set the working directory inside the container
WORKDIR /root/

# Copy the pre-built binary from the builder stage
COPY --from=builder /app/main .
COPY ./templates ./templates

# Expose the port the application will run on
EXPOSE 8080

# Run the binary when the container launches
CMD ["./main"]