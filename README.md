# TemplateJet API Documentation

## Overview

TemplateJet is a service designed to help users manage Handlebars (hbs) files effectively. It provides endpoints to render templates and manage templates.

## Base URL

All endpoints described in this documentation have the base URL of `http://localhost:8080`.

## Endpoints

### 1. Render Template

#### Endpoint

```
POST /render?template=template.hbs
```

#### Description

This endpoint allows users to render an hbs template by providing the template file name along with the necessary data in the request body.

#### Request Parameters

- `template` (required): The name of the Handlebars template file to render.

#### Request Body

```json
{
  "name": "John Doe",
  "age": 30,
  "hobbies": ["Reading", "Traveling", "Photography"],
  "address": {
    "street": "123 Main St",
    "city": "New York",
    "state": "NY",
    "zip": "10001"
  }
}
```

#### cURL Example

```bash
curl --location 'http://localhost:8080/render?template=template.hbs' \
--header 'Content-Type: application/json' \
--data '{
  "name": "John Doe",
  "age": 30,
  "hobbies": ["Reading", "Traveling", "Photography"],
  "address": {
    "street": "123 Main St",
    "city": "New York",
    "state": "NY",
    "zip": "10001"
  }
}'
```

#### Response

The endpoint returns the rendered template as a response.

---

### 2. Get Template Content

#### Endpoint

```
GET /template/{templateFileName}
```

#### Description

This endpoint retrieves the content of the specified template file.

#### Path Parameters

- `templateFileName` (required): The name of the Handlebars template file.

#### cURL Example

```bash
curl --location 'localhost:8080/template/template.hbs'
```

#### Response

The endpoint returns the content of the requested template file.

---

### 3. List Templates

#### Endpoint

```
GET /templates
```

#### Description

This endpoint retrieves a list of all available template files.

#### cURL Example

```bash
curl --location 'http://localhost:8080/templates'
```

#### Response

The endpoint returns a list of template file names.

---

### Additional Notes

- Ensure that the Handlebars templates are correctly formatted and stored in the appropriate directory accessible by the TemplateJet service.
- Handlebars templates can include placeholders that will be replaced with data provided in the request body during rendering.
- Error handling should be implemented to handle cases such as invalid template names or missing template files.